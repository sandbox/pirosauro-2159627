<?php
/**
 * @file form.inc.php
 * Form alter and theme preprocess functions.
 */

use Drupal\Core\Template\Attribute;

/**
 * Implements hook_theme_prepare_HOOK().
 */
function bootstrap_theme_prepare_input(&$variables) {
  // Process buttons, adding classes if necessary.
  if (in_array($variables['theme_hook_original'], array('input__submit', 'input__button'))) {
    // Reference the attributes array.
    $attributes = &$variables['element']['#attributes'];
    // Unset core added classes.
    foreach (array_keys($attributes['class'], 'button') as $key) {
      unset($attributes['class'][$key]);
    }
    foreach (array_keys($attributes['class'], 'button-primary') as $key) {
      unset($attributes['class'][$key]);
    }
    // Add default button class.
    $attributes['class'][] = 'btn';
    $attributes['class'][] = 'btn-default';
    // Add additional classes for buttons that contain certain text.
    if (!empty($variables['element']['#value'])) {
      $values = array(
        // Specific values.
        t('Save and add')       => 'btn-info',
        t('Add another item')   => 'btn-info',
        t('Add effect')         => 'btn-primary',
        t('Add and configure')  => 'btn-primary',
        t('Update style')       => 'btn-primary',
        t('Download feature')   => 'btn-primary',
        // General values.
        t('Save')     => 'btn-primary',
        t('Apply')    => 'btn-primary',
        t('Create')   => 'btn-primary',
        t('Confirm')  => 'btn-primary',
        t('Submit')   => 'btn-primary',
        t('Export')   => 'btn-primary',
        t('Import')   => 'btn-primary',
        t('Restore')  => 'btn-primary',
        t('Rebuild')  => 'btn-primary',
        t('Search')   => 'btn-primary',
        t('Add')      => 'btn-info',
        t('Update')   => 'btn-info',
        t('Delete')   => 'btn-danger',
        t('Remove')   => 'btn-danger',
      );
      foreach ($values as $value => $class) {
        if (strpos($variables['element']['#value'], $value) !== FALSE) {
          $attributes['class'][] = $class;
          break;
        }
      }
    }
    $variables['attributes'] = new Attribute($attributes);
  }
}
