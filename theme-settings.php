<?php

/**
 * Implements hook_form_system_theme_settings_alter().
 * @todo  more performance related settings
 */
function bootstrap_form_system_theme_settings_alter(&$form, $form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }
  // Bootstrap version
  $form['bootstrap_version'] = array(
    '#type' => 'select',
    '#title' => t('Bootstrap version'),
    '#options' => array(
      '3.0.0' => 'v3.0.0',
      '3.0.1' => 'v3.0.1',
      '3.0.2' => 'v3.0.2',
      '3.0.3' => 'v3.0.3',
    ),
    '#default_value' => theme_get_setting('bootstrap_version'),
  );
  // Permormance and Tweak settings group
  $form['performance'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Theme performance settings'),
  );
  // CDN
  $form['performance']['bootstrap_cdn'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use CDN to load in the bootstrap files'),
    '#default_value' => theme_get_setting('bootstrap_cdn'),
    '#description'   => t('Use CDN (a third party hosting server) to host the bootstrap files, Bootstrap Theme will not use the local CSS files anymore and instead the visitor will download them from ') . l('bootstrapcdn.com', 'http://bootstrapcdn.com')
                        .'<div class="alert alert-error">' . t('WARNING: this technique will give you a performance boost but will also make you dependant on a third party who has no obligations towards you concerning uptime and service quality.') . '</div>',
  );

  // more performance related settings coming soon
}
